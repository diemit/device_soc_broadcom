# Broadcom

#### 简介

树莓派3B采用BCM2835作为SOC，相关文件目录在device_board_soc_broadcom/bcm2835中。目前可以与树莓派4B的SOC(BCM2711)通用。

hardware文件中包含显示和GPU相关文件。
1.  device_board_soc_broadcom/hardware/display
2.  device_board_soc_broadcom/hardware/gpu



#### 相关仓

- [device/board/iscas](https://gitee.com/openharmony-sig/device_board_iscas)
- [vendor/iscas](https://gitee.com/openharmony-sig/vendor_iscas)
